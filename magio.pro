#-------------------------------------------------
#
# Project created by QtCreator 2014-06-09T08:59:33
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = magio
CONFIG   += console
CONFIG += c++11
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cc
