#include <QCoreApplication>
#include <QNetworkAccessManager>
#include <QNetworkCookieJar>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDateTime>
#include <QUrlQuery>
#include <QDebug>
#include <QLinkedList>
#include <QJsonDocument>
#include <QJsonObject>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QTime>

void Authenticate(const QString &username, const QString &password, const QScopedPointer<QNetworkAccessManager> &network);
void CheckLogged(const QScopedPointer<QNetworkAccessManager> &network);
void Reserve(const QScopedPointer<QNetworkAccessManager> &network);

static bool check_logged = false;

void Authenticate(const QString &username,
                  const QString &password,
                  const QScopedPointer<QNetworkAccessManager> &network)
{
  static const QString kAuthenticateUrl("http://magioplaz.zoznam.sk/rezervacie/authenticate?%1");

  QUrlQuery postData;
  postData.addQueryItem("username", username);
  postData.addQueryItem("password", password);

  QNetworkRequest req(kAuthenticateUrl.arg(QDateTime::currentMSecsSinceEpoch()));
  req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

  QNetworkReply *resp = network->post(req, postData.toString(QUrl::FullyEncoded).toUtf8());
  qDebug() << "Authenticating...";

  QObject::connect(resp, &QNetworkReply::finished, [resp, &network] {
    resp->deleteLater();

    if (resp->error()) {
      qWarning() << resp->errorString();
      return QCoreApplication::quit();
    }

    QVariantMap data = QJsonDocument::fromJson(resp->readAll()).object().toVariantMap();

    if (data.isEmpty()) {
      qDebug() << "...Login successful";
      return (check_logged) ? CheckLogged(network) : Reserve(network);
    }

    qWarning() << "Failed to login";
    if (data.contains("errorMessage")) {
      qWarning() << data["errorMessage"].toString();
    }

    QCoreApplication::quit();
  });
}

void CheckLogged(const QScopedPointer<QNetworkAccessManager> &network) {
  static const QString kIsLoggedUrl("http://magioplaz.zoznam.sk/rezervacie/isLogged?%1");

  QNetworkRequest req(kIsLoggedUrl.arg(QDateTime::currentMSecsSinceEpoch()));

  QNetworkReply *resp = network->get(req);
  qDebug() << "Checking if logged in...";

  QObject::connect(resp, &QNetworkReply::finished, [resp, &network] {
    resp->deleteLater();

    if (resp->error()) {
      qWarning() << resp->errorString();
      return QCoreApplication::quit();
    }

    QString data(resp->readAll().constData());
    if (data != "\"tkislan\"") {
      qWarning() << "isLogged returned:" << data;
      qWarning() << "expecting tkislan";
      QCoreApplication::quit();
    }

    qDebug() << "...Check successful";

    Reserve(network);
  });
}

class CourtIterator {
 public:
  CourtIterator() : courts_{ 41, 42 }, it_(courts_.begin()) {}

  inline int Next() {
    Q_ASSERT(it_ != courts_.end());
    return *it_++;
  }

  inline bool HasNext() { return it_ != courts_.end(); }
 private:
  QLinkedList<int> courts_;
  QLinkedList<int>::ConstIterator it_;
} court_it;

static int reservation_hour = 0;
static bool reservation_successful = false;

void Reserve(const QScopedPointer<QNetworkAccessManager> &network) {
  static const QString kReserveUrl("http://magioplaz.zoznam.sk/rezervacie/rezervuj/%1/%2/%3?%4");

  if (!court_it.HasNext()) return QCoreApplication::quit();

  QUrlQuery postData;
  postData.addQueryItem("comment", "");

  QNetworkRequest req(kReserveUrl
                      .arg(court_it.Next())
                      .arg(QDate::currentDate().addDays(1).toString("yyyy-MM-dd"))
                      .arg(reservation_hour)
                      .arg(QDateTime::currentMSecsSinceEpoch()));
  req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

  QNetworkReply *resp = network->post(req, postData.toString(QUrl::FullyEncoded).toUtf8());
  qDebug() << "Reserving court...";

  QObject::connect(resp, &QNetworkReply::finished, [resp, &network] {
    resp->deleteLater();

    if (resp->error()) {
      qWarning() << resp->errorString();
      return QCoreApplication::quit();
    }

    auto data = QJsonDocument::fromJson(resp->readAll().constData()).object().toVariantMap();

    if (data.contains("errorCode")) {
      int error_code = data["errorCode"].toDouble();
      qDebug() << resp->request().url();

      switch (error_code) {
        case 7:
          qDebug() << data["errorMessage"].toString();
          return Reserve(network);
        case 8:
          qDebug() << data["errorMessage"].toString();
          return QCoreApplication::quit();
        default:
          qDebug() << "Unknown error";
          qDebug() << error_code << ':' << data["errorMessage"].toString();
          return QCoreApplication::quit();
      }
    }

    if (!data.contains("termin")) {
      qDebug() << "Failed to reserve court";
      return Reserve(network);
    }

    qDebug() << "...Reservation successful - " << data["termin"].toString();

    reservation_successful = true;

    QCoreApplication::quit();
  });
}

class ScopedTimer {
 public:
  ScopedTimer() { time_.start(); }
  ~ScopedTimer() { qDebug() << "Finished in" << time_.elapsed() << "milliseconds"; }
 private:
  QTime time_;
};

int main(int argc, char *argv[]) {
  ScopedTimer timer;

  QCoreApplication app(argc, argv);
  app.setApplicationName("Magio Reservations");
  app.setApplicationVersion("0.1.0");
  app.setOrganizationDomain("tkislan.org");
  app.setOrganizationName("TKislan");

  QCommandLineOption hour_option({ "r", "reservation_hour" },
                                 "Hour of reservation",
                                 "hour");

  QCommandLineOption username_option({ "u", "username" },
                                     "Username",
                                     "username");

  QCommandLineOption password_option({ "p", "password" },
                                     "Password",
                                     "password");

  QCommandLineOption check_logged_option({ "c", "check_logged" },
                                         "Add check if logged in");

  QCommandLineParser parser;
  parser.setApplicationDescription("Magio Beach beach volleyball court reservation bot");
  parser.addHelpOption();
  parser.addOption(hour_option);
  parser.addOption(username_option);
  parser.addOption(password_option);
  parser.addOption(check_logged_option);

  parser.process(app);

  check_logged = parser.isSet(check_logged_option);

  if (!parser.isSet(hour_option)
      || !parser.isSet(username_option)
      || !parser.isSet(password_option))
  {
    qWarning() << "Missing argument";
    qWarning() << parser.helpText();
    return -1;
  }

  QString username, password;

  reservation_hour = parser.value(hour_option).toInt();
  username = parser.value(username_option);
  password = parser.value(password_option);

  if (reservation_hour < 9 || reservation_hour > 21) {
    qWarning() << "Hour argument should be between 9 and 21";
    return -1;
  }

  QScopedPointer<QNetworkAccessManager> network;
  network.reset(new QNetworkAccessManager);
  network->setCookieJar(new QNetworkCookieJar(network.data()));

  Authenticate(username, password, network);

  int ret = app.exec();

  if (ret != 0) return ret;

  if (!reservation_successful) {
    qDebug() << "Failed to reserve court";
  }

  return reservation_successful ? 0 : 1;
}
